import praw
import random
import time
import urllib2

REDDIT_USERNAME = ''
REDDIT_PASS = ''
SUBREDDIT_NAME = 'uplifitingmessages'

MESSAGE_SUBJECT = "You're post has been linked on /r/uplifitingmessages"
MESSAGE_BODY = """
Please report any harrassment immediately, and have a nice day!
"""

PULL_SUBS = ['Depression', 'SuicideWatch', 'OffMyChest']

def pull_post(r, sub):
    submissions = sub.get_new(limit = 100)
    index = random.randrange(1, 99)

    i = 1
    for submission in submissions:
        if i == index:
            return submission
        else:
            i += 1

def post_to_sub(r, sub, title, pull_sub_name, url):
    post_title = "[/r/{}] {}".format(pull_sub_name, title)

    while True:
        try:
            r.submit(sub, post_title, text = None, url=url)
            return
        except urllib2.HTTPError, e:
            if e.code in [429, 500, 502, 503, 504]:
                print "Reddit is down (Error: {}), sleeping...".format(e.code)
                time.sleep(60)
                pass

def message_user(r, user):
    sub = MESSAGE_SUBJECT
    msg = MESSAGE_BODY

    while True:
        try:
            r.send_message(user, sub, msg, captcha=None)
            return
        except urllib2.HTTPError, e:
            if e.code in [429, 500, 502, 503, 504]:
                print "Reddit is down (Error: {}), sleeping...".format(e.code)
                time.sleep(60)
                pass

def main():
    print "Logging in..."
    r = praw.Reddit(user_agent = 'UpliftingMessages v0.1')
    r.login(REDDIT_USERNAME, REDDIT_PASS, disable_warning = True)

    while True:
        while True:
            index = random.randrange(0, 3)
            try:
                pull_sub = r.get_subreddit(PULL_SUBS[index])
                post_sub = r.get_subreddit(SUBREDDIT_NAME)

                print "Finding a post..."
                post = pull_post(r, pull_sub)
                break
            except urllib2.HTTPError, e:
                if e.code in [429, 500, 502, 503, 504]:
                    print "Reddit is down (Error: {}), sleeping...".format(e.code)
                    time.sleep(60)
                    pass

        print "Posting to sub..."
        post_to_sub(r, post_sub, str(post.title), PULL_SUBS[index], str(post.url))
        print "Messaging user..."
        message_user(r, str(post.author))

        print "Sleeping..."
        time.sleep(7200)

if __name__ == "__main__":
    main()
