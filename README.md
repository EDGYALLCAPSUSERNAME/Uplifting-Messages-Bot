UpliftingMessages Bot
=====================

Setup
-----

This script requires python 2.7 and praw.

To install praw type in your command line:

    pip install praw

After that open up the uplifting_bot.py file and edit the
REDDIT_USERNAME and REDDIT_PASS with the username and password of the account you want to post.

You can edit the subject and the message sent to the user whose post was xposted by changing what is in the MESSAGE_BODY and MESSAGE_SUBJECT variables.
